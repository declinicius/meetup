export interface IEntrada {
	id : number;
    title : string; 
    subtitle : string;
    body : string;
    icon : string;
    clicks:number;
    visitada:string;
    colorNavegacion:string;
    mainImage:string;
    content:any;
}
