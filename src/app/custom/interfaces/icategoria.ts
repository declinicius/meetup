export interface ICategoria {
	nombre : string;
	ruta :	string;
	src:string;
	color: string;
  	subcategorias : any[];
  	icon : string;
}
