import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from '../core/core.module';
//imports basicos
import { AppRoutingModule } from '@app/app-routing.module';

//imports para la bbdd en memoria
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { EntradaComponent } from './components/tutorial/entrada/entrada.component';
import { TemarioComponent } from './components/tutorial/temario/temario.component';
import { EntradaDialogComponent } from './components/tutorial/temario/entrada-dialog/entrada-dialog.component';

//Services
import { TutorialService }          from '@dcl-custom/services/tutorial.service';
import { InMemoryDataService }  from '@dcl-custom/services/in-memory-data.service';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    SharedModule,
    AppRoutingModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )

  ],
  declarations: [ 
  	EntradaComponent,
    TemarioComponent,
    EntradaDialogComponent],
    providers: [  TutorialService ],
    exports: [
    	EntradaComponent,
    	TemarioComponent,
    	EntradaDialogComponent
    ],
	entryComponents: [EntradaDialogComponent]

})
export class CustomModule { }
