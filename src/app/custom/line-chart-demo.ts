export class LineChartDemo {

    data: any;
    options: any;

    constructor() {
        this.data = {
            labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio'],
            datasets: [
                {
                    label: 'Ingresos',
                    data: [65, 59, 80, 81, 56, 55, 40]
                },
                {
                    label: 'Pérdidas',
                    data: [28, 48, 40, 19, 86, 27, 90]
                }
            ]
        };

        this.options = {
            title: {
                display: true,
                text: 'Ingresos vs Pérdidas',
                fontSize: 16
            },
            legend: {
                position: 'bottom'
            }
        };
    }
}