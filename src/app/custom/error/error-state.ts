import {Component} from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';


/** Error when invalid control is dirty, touched, or submitted. */
export class ErrorState implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

export class EmailErrorStateMatcher {
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email
  ]);

  matcher = new ErrorState();
}

export class MaxLengthErrorStateMatcher {
  maxLengthFormControl = new FormControl( [
    Validators.required,
    Validators.maxLength
  ]);

  matcher = new ErrorState();
}

