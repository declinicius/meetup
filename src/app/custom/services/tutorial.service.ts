import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from '@dcl-services/message.service';
import { IEntrada } from '@dcl-custom/interfaces/ientrada';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class TutorialService {

    private tutorialUrl = 'api/entradas';  // URL al web api

    constructor(
      private http: HttpClient,
      private messageService: MessageService) { }

    /** GET - Recuperamos las entradas del servidor */
    getEntradas (): Observable<IEntrada[]> {
      return this.http.get<IEntrada[]>(this.tutorialUrl)
        .pipe( 
          tap(entradas => this.log(`Hemos recuperado el contenido del MeetUp!`)),
          catchError(this.capturaError('getEntradas', []))
        );
    }

    /** GET - Entrada id. Devuelve `undefined`cuando no se encuentra */
    getDefaultNo404<Data>(id: number): Observable<IEntrada> {
      const url = `${this.tutorialUrl}/?id=${id}`;
      return this.http.get<IEntrada[]>(url)
        .pipe(
          map(entradas => entradas[0]), // devuelve {0|1} 
          tap(salidaResultados => {
            const salida = salidaResultados ? `recuperado` : `no se ha encontrado`;
            this.log(`${salida} -> entrada id=${id}`);
          }),
          catchError(this.capturaError<IEntrada>(`Error recuperando el Tutorial con id=${id}`))
        );
    }

    /** GET -- Entrada por id. Devuelve 404 si no la encuentra */
    getEntrada(entradaId: number): Observable<IEntrada> {
      const url = `${this.tutorialUrl}/${entradaId}`;
      return this.http.get<IEntrada>(url).pipe(
        tap(_ => this.log(`recuperado el la entrada con id=${entradaId}`)),
        catchError(this.capturaError<IEntrada>(`getEntrada ha cascado al recuperar id=${entradaId}`))
      );
    }

    /* BusquedaPorTitulo busca los tutoriales que contienen dicho termino */
    busquedaPorTitulo(palabra: string): Observable<IEntrada[]> {
      if (!palabra.trim()) {
        // devuelve 0 en caso de no encontrar nada.
        return of([]);
      }
      return this.http.get<IEntrada[]>(`${this.tutorialUrl}?title=${palabra}`).pipe(
        tap(_ => this.log(`ha encontrado estos títulos por la palabra "${palabra}"`)),
        catchError(this.capturaError<IEntrada[]>('busquedaPorTitulo', []))
      );
    }

    //////// Save - Guardar //////////

    /** POST: añade una nueva entrada a la BBDD temporal*/
    guardarEntrada(entrada: IEntrada): Observable<IEntrada> {
      console.log("guardar Entrada");
      console.log(entrada);
      return this.http.post<IEntrada>(this.tutorialUrl, entrada, httpOptions).pipe(
        tap((entrada: IEntrada) => this.log(`guardada Entrada IEntrada w/ id=${entrada.id}`)),
        catchError(this.capturaError<IEntrada>('guardarEntrada'))
      );
    }

    /** DELETE: Borrar entrada del servidor */
    borrarEntrada (entrada: IEntrada | number): Observable<IEntrada> {
      const id = typeof entrada === 'number' ? entrada : entrada.id;
      const url = `${this.tutorialUrl}/${id}`;

      return this.http.delete<IEntrada>(url, httpOptions).pipe(
        tap(_ => this.log(`entrada eliminada id=${id}`)),
        catchError(this.capturaError<IEntrada>('borrarEntrada'))
      );
    }
    
    /** PUT: Actualiza entrada en el servidor */
    actualizaEntrada (entrada: IEntrada): Observable<any> {
      return this.http.put(this.tutorialUrl, entrada, httpOptions).pipe(
        tap(_ => this.log(`actualiza entrada con el id=${entrada.id}`)),
        catchError(this.capturaError<any>('actualizaEntrada'))
      );
    }

    private capturaError<T> (operacion = 'operation', resultado?: T) {
      return (error: any): Observable<T> => {

        
        console.error(error); // log a consola

        this.log(`${operacion} fallida: ${error.message}`);

        // Permitimos que la aplicación continue a pesar del error, devolviendo valor vacío.
        return of(resultado as T);
      };
    }

    private log(message: string) {
        this.messageService.add('TutorialService: ' + message);
    }
  }
 