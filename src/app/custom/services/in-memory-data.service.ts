import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const entradas = [
       { id: 1,
       title: '¿Qué es Angular?', 
       colorNavegacion:'second',
       subtitle:'Angular es un framework de programación basado en JavaScript',
       body:'Nos permite hacer todo lo que podamos imaginar con una sola implementación de nuestro código.\n Nos permite reaccionar a los eventos relacionadas con la interacción del usuario tanto en web como en dispositivos móviles. \n Podemos olvidarnos de muchos problemas.',
       content:{
           views:[
              {text:'JS',img:'assets/examples/2/JS.png'},
               {text:'TypeScript',img:'assets/examples/2/TypeScript.jpg'},
               {text:'npm',img:'assets/examples/2/npm.jpg'},
               {text:'NodeJS',img:'assets/examples/2/node.png'}
               ]
             },
       clicks:0},
      { id: 2,
        mainImage: 'assets/examples/1/mainImage.jpg',
        colorNavegacion:'coral',
        title: 'Estructura de Angular',
        subtitle: 'Comportamiento',
        body:'Desde el fichero index.html, importamos el componente app-root, "bootsrapeado" desde el app.module.ts, permitiendo cargar el resto de la aplicación.' , 
        clicks:0, 
        content:{
          views:[]
        }},
      { id: 3,
       mainImage: '',  
       colorNavegacion:'coral',
       title: 'Hazlo fácil!',
       subtitle: '¿Por qué usar Angular?',
       body:'Porque puedes programar con el mismo framework y usarlo tanto para móvil como para web sin tener que rompernte la cabeza.',
       content :{
         views:[
               {text:'Sencillo',img:'assets/examples/1/sencillo.jpg'},
               {text:'Organizado',img:'assets/examples/1/organized.jpg'},
               {text:'Escalable',img:'assets/examples/1/escalable.jpg'},
               {text:'Rápido',img:'assets/examples/1/fast.png'}
               ]},
       clicks:0},
      { id: 4, 
        mainImage:'assets/examples/4/AngularCli.jpg',
        colorNavigation:'cli',
        title:'Angular CLI',
        subtitle: 'Con NodeJS instalado y 4 instrucciones, ya podemos empezar a trabajar!',
        body:'Angular CLI nos permite crear componentes, servicios, módulos, directivas, etc.', 
        clicks:0,
        content:{views:{}}
       },
       { id: 5, 
        mainImage: '',  
        colorNavegacion:'coral',
        title: 'Full-Framework', 
        subtitle:'Angular es un Framework completo', 
        body:'Tenemos todas las herramientas disponibles en cualquier componente para llamar a BDD, WS y a otros componentes de nuestra aplicación, a través de la importación de servicios, módulos y otros componentes.',
        content:{
            views:[
               {text:'Templates',img:'assets/examples/3/view.jpg'},
               {text:'Database',img:'assets/examples/3/database.png'},
               {text:'Servicios',img:'assets/examples/3/services.png'},
               {text:'Enrutador',img:'assets/examples/3/routing.png'}, 
               {text:'HTTP requests',img:'assets/examples/3/http.png'}
               ]
             }, 
        clicks:0},
      { id: 6,
        mainImage: null,
        colorNavegacion:'second',
        title: 'Cicle de vida',
        subtitle: 'Los componentes en sus distintos estados',
        body:`Un componente tiene un ciclo de vida administrado por Angular.\nRenderiza, crea y representa sus hijos, lo revisa cuando cambian sus propiedades de datos y lo destruye antes de eliminarlo del DOM.\n'+
       'Angular ofrece ganchos de ciclo de vida que ofrecen visibilidad en estos momentos\nclaves de la vida y la capacidad de actuar cuando ocurren.\nLas directivas tienen el mismo conjunto de ganchos de ciclo de vida.` ,
        clicks:0, 
        content:{views:{}}},
      { id: 7,
        mainImage:'assets/examples/6/webpack.jpg',
        colorNavegacion:'black',
        title: 'Empaquetador de módulos', 
        subtitle: 'Webpack', 
        body:'Webpack es un module bundler (empaquetador de módulos) muy eficiente para aplicaciones grandes que contienen mucho código JavaScript. En cierta medida se puede considerar un Browserify avanzado ya que tiene muchas opciones de configuración.' , 
        clicks:0, 
        content:{}},
       {
        id:8,
        mainImage: null,  
        colorNavegacion:'second',
        title: 'Fácil de usar',
        subtitle: 'Con un solo comando "ng g c miComponente"',
        body:'Reaprovechamos el código. Delegamos la responsabilidad a los otros componentes. Al crear un componente disponemos de todo lo necesario para poder trabajar. Creamos elementos ya renderizables con un solo comando.',
        clicks:0, 
        content:{
          
        }}];

    return {entradas};
  }
}
