import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { IEntrada } from '@dcl-custom/interfaces/ientrada';
import { TutorialService }  from '@dcl-custom/services/tutorial.service';
import { TopNavService }  from '@dcl-services/top-nav.service';
import { categorias, categoriasLentas, listItems } from '@dcl-animations/enterLeave.animations';
import { aparecer }  from '@dcl-animations/fade.animations';
import { slideIn } from '@dcl-animations/slide-in.animations';
import { MatCard, MatExpansionPanel,MatExpansionPanelHeader,MatExpansionPanelContent } from '@angular/material';

@Component({
  selector: 'entrada-detalle',
  templateUrl: './entrada.component.html',
  styleUrls: ['./entrada.component.scss'],
  animations : [ categorias, categoriasLentas, aparecer, listItems ]
})
export class EntradaComponent implements OnInit {
  @Input() entrada: IEntrada; 
  defaultColor : string = 'second';

  constructor(
    private route: ActivatedRoute,
    private tutorialService: TutorialService,
    private topNavService: TopNavService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getEntrada();
  }

  getEntrada(): void {
    var id = +this.route.snapshot.paramMap.get('id');
    this.callLink(id);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.tutorialService.actualizaEntrada(this.entrada)
      .subscribe(() => this.goBack());
  }

  callLink(id:number){
    this.entrada = null;
     this.tutorialService.getEntrada(id)
      .subscribe(response => {
         this.entrada = response;
         if(response.title){
           this.topNavService.cambiarTitulo(response.title);
         }
         var color = this.defaultColor;
         if(response.colorNavegacion){
            color = response.colorNavegacion;
         }
         this.topNavService.cambiarColor(color); 
      }); 
    }  
}
