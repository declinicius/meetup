import { Component, OnInit, Inject , Input} from '@angular/core';
import { FormGroup, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MatCard, MatDialogRef,  MatFormFieldControl, MatInputModule, MAT_DIALOG_DATA } from '@angular/material';
import { TemarioComponent } from '../temario.component';
import { IEntrada } from '@dcl-custom/interfaces/ientrada';
import { ErrorStateMatcher } from '@angular/material/core';
import { ErrorState } from '@dcl-custom/error/error-state';
 
@Component({
  selector: 'entrada-dialog',
  templateUrl: './entrada-dialog.component.html',
  styleUrls: ['./entrada-dialog.component.scss']
})
 
export class EntradaDialogComponent implements OnInit {
  entrada: IEntrada;
  form: FormGroup;


  constructor(
    private formBuilder: FormBuilder,
    public miDialogo: MatDialogRef<TemarioComponent>,
     @Inject(MAT_DIALOG_DATA) public data: any) {
  		this.entrada = data.entrada;
      this.sumaClicks();
    }

    ngOnInit() {
      this.form = this.formBuilder.group({
        titulo: this.entrada.title,
        subtitulo: this.entrada.subtitle,
        cuerpo: this.entrada.body
      })
    }
    onNoClick(): void {
      this.miDialogo.close();
    }

    sumaClicks(){
    		this.entrada.clicks++;	
  	}

    guardarEntrada(form){
     this.miDialogo.close(form);
    }


}
