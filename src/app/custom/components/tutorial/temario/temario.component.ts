import { Component, OnInit , Inject, ViewChild } from '@angular/core';
import { IEntrada } from '@dcl-custom/interfaces/ientrada';
import { TutorialService }  from '@dcl-custom/services/tutorial.service';
import { TopNavService }  from '@dcl-services/top-nav.service';
import { MatToolbar, MatDialog, MatButton } from '@angular/material';
import { EntradaDialogComponent } from './entrada-dialog/entrada-dialog.component';
import { trigger,style,transition,state,animate,keyframes,query,stagger } from '@angular/animations';
import { listItems, entrarSalirAnimation } from '@dcl-animations/enterLeave.animations';
import { MatList, MatListItem, MatIconModule } from '@angular/material';

@Component({
  selector: 'temario',
  templateUrl: './temario.component.html',
  styleUrls: [ './temario.component.scss' ],
  animations: [ entrarSalirAnimation , listItems]
})

export class TemarioComponent implements OnInit {
  tutorial: IEntrada[] = [];
  @ViewChild('entrada') public entrada : IEntrada;
  clicks : number = 0;
  
  constructor(
    private tutorialService: TutorialService,
    private topNavService:TopNavService,
    public dialogEntrada: MatDialog) {

  }

  ngOnInit() {
    this.topNavService.cambiarTitulo('Contenido');
    this.topNavService.cambiarColor('second');
    this.getTutorial();
    this.clicks = 0;
  }

  getTutorial(): void {
    this.tutorialService.getEntradas()
      .subscribe(tutorial => this.tutorial = tutorial);
  }

  setSelectedEntrada(entradaSeleccionada: IEntrada){
    this.entrada = entradaSeleccionada;
  }
  
  openEntradaDialog(entrada:IEntrada): void {
    this.entrada = entrada;
    this.entrada.visitada = 'activa';
    if(!this.entrada.clicks){
      this.entrada.clicks = 0;
    }
    //alert(this.entrada.title);
    let dialogRef = this.dialogEntrada.open(EntradaDialogComponent, {
      width: '80%',
      height: '90%',
      data: { entrada : this.entrada }
    });

    dialogRef.beforeClose().subscribe(result => {
      console.log("--- result dialog ---");
      console.log(result);
     
      this.clicks++;
      if(result){
        this.entrada.title = result.value.titulo;
        this.entrada.subtitle = result.value.subtitulo;
        this.entrada.body = result.value.cuerpo;
         console.log("--- actualizar entrada ---");
        this.tutorialService.actualizaEntrada(this.entrada);

      }
      this.entrada.visitada = 'visitada';
      this.limpiaEntrada();
      console.log("--- end result dialog ---");
    });
  }


  limpiaEntrada(){
    this.setSelectedEntrada(null);
  }

  aumentaClicks(){
    this.clicks++;
  }

}
