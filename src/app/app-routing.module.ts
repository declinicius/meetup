import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WelcomeComponent }   from '@dcl-core/welcome.component';
import { PageNotFoundComponent }   from '@dcl-core/page-not-found.component';
import { TemarioComponent }   from '@dcl-custom/components/tutorial/temario/temario.component';
import { EntradaComponent }  from '@dcl-custom/components/tutorial/entrada/entrada.component';


//urls para dar respuesta en routeroutlet
const routes: Routes = [
  { path: '', redirectTo: '/welcome', pathMatch: 'full' },
  { path: 'tutorial', component: TemarioComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'tutorial/detalle/:id', component: EntradaComponent},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class  AppRoutingModule{}