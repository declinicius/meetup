import { trigger, animate, transition, style, query, state, stagger, animateChild } from '@angular/animations';

export const routeState =
  trigger('routeState', [
   // route 'enter and leave (<=>)' transition
    transition('*<=>*', [

      // css styles at start of transition
      style({ opacity: 0 }),

      // animation and styles at end of transition
      animate('0.4s', style({ opacity: 1 }))
    ])
  ]);

  export const aparecer = 
   trigger('aparecer', [
      transition(":enter", [
        query('div',[
          style({ opacity: 0 }),
          animate(500, style({ opacity: 1 }))
        ]),
        query('@categoriasLentas',[
          animateChild()
        ])
      ]),
     transition(":leave", [
      animate(500, style({ opacity: 0 }))
      ])
     ]);
