
import { trigger, animate, transition, style, query, state, stagger,group } from '@angular/animations';

export const entrarSalirAnimation =

    trigger('entrarSalir', [
    state('in', style({transform: 'translateX(0)'})),
    transition('void => *', [
      style({transform: 'translateX(-100%)'}),
      animate(1000)
    ]),
    transition('* => void', [
      animate(1000, style({transform: 'translateX(100%)'}))
    ])
  ]);

  export const categorias = 
   trigger('categorias', [
      transition('* => *', [
        query('div',style({ transform: 'scale(0.2)', opacity: 0 }),{optional:true}),
        query('div',
          stagger(50, [
            animate('300ms', style({transform: 'scale(1)', opacity: 1 }))
        ]),{optional:true})
      ])
    ]);

   export const categoriasLentas = 
    trigger('categoriasLentas', [
      transition('* => *', [
        query('div',style({ transform: 'scale(0.2)', opacity: 0 }),{optional:true}),
        query('div',
          stagger(500, [
            animate('300ms', style({transform: 'scale(1)', opacity: 1 }))
        ]),{optional:true})
      ])
    ]);

  export const listItems = 
    trigger('tutorialItems', [
      transition('* => *', [
        query('div',style({ transform: 'scale(0.2)', opacity: 0 }),{optional:true}),
        query('div',
          stagger(50, [
            animate('300ms', style({transform: 'scale(1)', opacity: 1 }))
        ]),{optional:true})
      ])
    ]);

export const incrementable = 
 trigger('incrementable', [
   transition(":increment", group([
        query(':enter', [
          style({ left: '100%' }),
            animate('0.5s ease-out', style('*'))
          ]),
          query(':leave', [
            animate('0.5s ease-out', style({ left: '-100%' }))
          ])
        ])),
        transition(":decrement", group([
          query(':enter', [
            style({ left: '-100%' }),
            animate('0.5s ease-out', style('*'))
          ]),
           query(':leave', [
              animate('0.5s ease-out', style({ left: '100%' }))
            ])
        ]))
    ]);