import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'vista',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  @Input() miVista:any;
  constructor() { }

  ngOnInit() {
  }

}
