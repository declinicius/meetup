import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser'; 
import { CommonModule } from '@angular/common';
import { ViewComponent } from './view/view.component';


//imports para formas prediseñadas
import { MatMenuModule, MatSidenavModule, MatButtonModule,  MatInputModule, MatToolbarModule, MatDialogModule, MatCardModule,
MatIconRegistry , MatListModule, MatIconModule, MatFormFieldModule, MatExpansionModule, MatOptionModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    MatMenuModule, MatSidenavModule, MatButtonModule, 
    MatToolbarModule, MatIconModule, MatDialogModule, 
    MatCardModule, MatFormFieldModule, MatOptionModule, MatListModule,
    MatInputModule, MatExpansionModule
    
  ],
  declarations: [ViewComponent]	,
  exports: [
    MatMenuModule, MatSidenavModule, MatButtonModule, 
    MatToolbarModule, MatIconModule, MatDialogModule, 
    MatCardModule, MatFormFieldModule, MatOptionModule, MatListModule,
    MatExpansionModule,
    MatInputModule, ViewComponent
  ]
 
})
export class SharedModule { }
