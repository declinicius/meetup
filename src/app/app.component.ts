import { Component, OnInit, ViewChild, HostBinding } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';
import { SideNavsService } from '@dcl-services/side-navs.service';
import { BrowserModule }  from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { routeState} from '@dcl-animations/fade.animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [routeState]
})

export class AppComponent implements OnInit {
  @ViewChild('leftSideNav') public leftSideNav: MatSidenav;	
  tituloAplicacion = 'Angular MeetUp!';
  name:string;

  constructor(private sideNavsService: SideNavsService ){
  	this.sideNavsService.setLeftSidenav(this.leftSideNav);
  }

  ngOnInit() {
    // Store sidenav to service
    this.sideNavsService
      .setLeftSidenav(this.leftSideNav);
  }
  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
  public getState(outlet) {
    return outlet.activatedRouteData.state;
  }
}
