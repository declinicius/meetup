import { NgModule }       from '@angular/core';
/*Modules*/
import { CommonModule } from '@angular/common';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { CustomModule} from './custom/custom.module';

/*imports sobre componentes de la aplicación*/
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    SharedModule,
    CustomModule
  ],
  /** providers permite publicar los servicios de los cuales se alimentan los componentes */
   bootstrap: [AppComponent]
})
export class AppModule { }


