import { TestBed, inject } from '@angular/core/testing';

import { SideNavsService } from './side-navs.service';

describe('SideNavsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SideNavsService]
    });
  });

  it('should be created', inject([SideNavsService], (service: SideNavsService) => {
    expect(service).toBeTruthy();
  }));
});
