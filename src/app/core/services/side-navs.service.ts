import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material';


@Injectable()
export class SideNavsService {
  private leftSideNav: MatSidenav;

  
  public setLeftSidenav(sidenav: MatSidenav) {
    this.leftSideNav = sidenav;
  }

  public open() {
     this.leftSideNav.open();
  }

  public close(){
    return this.leftSideNav.close();
  }

}
