import { TestBed, inject } from '@angular/core/testing';

import { TopNavService } from './top-nav.service';

describe('TopNavService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TopNavService]
    });
  });

  it('should be created', inject([TopNavService], (service: TopNavService) => {
    expect(service).toBeTruthy();
  }));
});
