import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TopNavService {
  private titulo:Subject<string> = new Subject<string>();
  private color:Subject<any> = new Subject<any>();
  constructor() { 
   
  }
  public getColor() : Observable<any> {
    return this.color.asObservable();
  }
  
  public cambiarColor(color:string){
    console.log("nos llega petición para cambiar título a "+color);
    this.color.next({ text:color});
  }

  public cambiarTitulo(titulo:string){
     this.titulo.next(titulo);
  }

  public limpiarTitulo(){
        this.titulo.next();
  }

  public getTitulo() : Observable<string> {
        return this.titulo.asObservable();
    }
    
}
