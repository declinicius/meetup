import { NgModule } from '@angular/core';

import { BrowserModule }  from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { FormsModule, ReactiveFormsModule }    from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from '../app-routing.module';

import { CommonModule } from '@angular/common';
import { WelcomeComponent } from '@dcl-core/welcome.component';
import { DemoBottomComponent } from '@dcl-core/demo-bottom.component';
import { DemoHeaderComponent } from '@dcl-core/demo-header.component';
import { DemoSidenavLeftComponent } from '@dcl-core/demo-sidenav-left.component';
import { DemoSidenavRightComponent } from '@dcl-core/demo-sidenav-right.component';
import { PageNotFoundComponent } from '@dcl-core/page-not-found.component'
import { MessagesComponent } from '@dcl-core/messages.component';

import { MessageService }  from '@dcl-services/message.service';
import { TopNavService }   from '@dcl-services/top-nav.service';
import { SideNavsService } from '@dcl-services/side-navs.service';

@NgModule({
  declarations: [
  	DemoBottomComponent,
    DemoHeaderComponent,
    DemoSidenavLeftComponent,
    DemoSidenavRightComponent,
    WelcomeComponent,
    PageNotFoundComponent,
    MessagesComponent
  ],
  imports: [
    AppRoutingModule,
    CommonModule,
    BrowserModule, 
    BrowserAnimationsModule,
    FormsModule, ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    SharedModule 
  ],
  providers: [ MessageService, TopNavService, SideNavsService],
  exports : [
  	CommonModule,
  	AppRoutingModule,
    DemoBottomComponent,
    DemoHeaderComponent,
    DemoSidenavLeftComponent,
    DemoSidenavRightComponent,
    WelcomeComponent,
    FormsModule, ReactiveFormsModule,
    PageNotFoundComponent,
    MessagesComponent]
})
export class CoreModule { }
