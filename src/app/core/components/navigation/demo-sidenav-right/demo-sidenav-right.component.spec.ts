import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoSidenavRightComponent } from './demo-sidenav-right.component';

describe('DemoSidenavRightComponent', () => {
  let component: DemoSidenavRightComponent;
  let fixture: ComponentFixture<DemoSidenavRightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoSidenavRightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoSidenavRightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
