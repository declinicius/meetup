import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MatSidenav, MatSidenavModule, MatIconBase, MatIcon } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { SideNavsService } from '@dcl-services/side-navs.service';
import { TopNavService } from '@dcl-services/top-nav.service';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
//import { routeState } from '../../../animations/fade.animations';

@Component({
  selector: 'demo-header',
  templateUrl: './demo-header.component.html',
  styleUrls: ['./demo-header.component.scss'] /*,
  animations : [routeState]*/
})

export class DemoHeaderComponent implements OnInit {
  @Input() name: string;
  subscriptionTitulo: Subscription;
  subscriptionColor: Subscription;
  titulo:string;
  color:string;
  
  constructor(
    private sideNavsService : SideNavsService,
    private topNavService : TopNavService) {}

  ngOnInit() {
      this.color = 'second';
      this.topNavService.cambiarTitulo('MeetUp Angular!');
      this.subscriptionTitulo = this.topNavService.getTitulo().subscribe(
          titulo => {
            this.titulo = titulo;
          }
       );
      this.subscriptionColor = this.topNavService.getColor().subscribe(
          color => {
            this.color = color.text;
            console.log(color.text);
          }
       );
  }

  openSideNav(){
  	this.sideNavsService.open();
  }

  onDestroy(){
      this.subscriptionTitulo.unsubscribe();
      this.subscriptionColor.unsubscribe();
  }
}
