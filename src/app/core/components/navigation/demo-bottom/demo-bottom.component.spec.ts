import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoBottomComponent } from './demo-bottom.component';

describe('DemoBottomComponent', () => {
  let component: DemoBottomComponent;
  let fixture: ComponentFixture<DemoBottomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoBottomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
