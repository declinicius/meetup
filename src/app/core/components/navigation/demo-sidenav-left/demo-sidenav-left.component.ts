import { Component, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterOutlet} from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ICategoria } from '../../../../custom/interfaces/icategoria';
import { trigger,style,transition,animate,keyframes,query,stagger } from '@angular/animations';
import { MatMenu, MatButtonModule, MatToolbar, MatIcon } from '@angular/material';
import { SideNavsService } from '../../../services/side-navs.service';

@Component({
  selector: 'demo-sidenav-left',
  templateUrl: './demo-sidenav-left.component.html',
  styleUrls: ['./demo-sidenav-left.component.scss']
})
             
export class DemoSidenavLeftComponent implements OnInit {
  state : boolean;
  enlaces : ICategoria[];
  titulo : any;
  total : number;

  constructor(
    private sideNavsService : SideNavsService
    ) { }

  ngOnInit() {
  		this.enlaces = [	
   		{nombre:"Welcome",   ruta:"/welcome",   color: "second" , icon:"accessibility", subcategorias : null,src:null},
      {nombre:"Tutorial", ruta:"/tutorial", color: "second", icon:"book", subcategorias : null, src:null},
   		{nombre:"Trello", 	ruta:null, src:"https://trello.com/b/cDc6jilC/angular-examples", color: "second", icon:"trello", subcategorias : null},
   		{nombre:"Angular.io", ruta:null, src:"https://angular.io/",	color: "primary", icon:"angular", subcategorias : null},
      {nombre:"Angular Material", ruta:null, src:"https://material.angular.io/",  color: "primary", icon:"material-ui", subcategorias : null}];
  }
  
  cerrar(){
    this.sideNavsService.close();
  }
}

