import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoSidenavLeftComponent } from './demo-sidenav-left.component';

describe('DemoSidenavLeftComponent', () => {
  let component: DemoSidenavLeftComponent;
  let fixture: ComponentFixture<DemoSidenavLeftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoSidenavLeftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoSidenavLeftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
