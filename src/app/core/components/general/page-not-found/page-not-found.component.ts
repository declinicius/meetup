import { Component, OnInit } from '@angular/core';
import { TopNavService }  from '@dcl-services/top-nav.service';
import { MatCard } from '@angular/material';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {
  public mensaje:string;

  constructor(private topNavService: TopNavService) { 
  	this.mensaje = "Esta ruta te llevará lejos, pero en la dirección equivocada.";
  }

  ngOnInit() {
  		this.topNavService.cambiarTitulo("Los dinosaurios se extinguieron por mucho menos.");
  		this.topNavService.cambiarColor('red');
  }

}
