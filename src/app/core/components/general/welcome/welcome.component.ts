import { BrowserModule } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { TopNavService } from '@dcl-services/top-nav.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { routeState} from '@dcl-animations/fade.animations';

@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
  animations: [ routeState ]
})
export class WelcomeComponent implements OnInit {
   
  constructor(
   private topNavService : TopNavService) {}

  ngOnInit() {
      this.topNavService.cambiarTitulo('MeetUp Angular!');
      this.topNavService.cambiarColor('second');
  }

}
